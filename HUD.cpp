#include "HUD.h"
#include "player.h"
#include <string>
#include <iostream>

HUD::HUD(player &Player) : Player(Player)
{
	font.loadFromFile("data/Fonts/Pacha.otf");
	Gold_1.setFont(font);
	Gold_1.setCharacterSize(24);
	Gold_1.setColor(Color(170, 160, 57));

	statistic[0].setFillColor(Color(82, 0, 10));
	statistic[1].setFillColor(Color(22, 39, 86));
	statistic[2].setFillColor(Color(236, 248, 165));

	valueOfStatistic[0] = Player.getHP(); 
	valueOfStatistic[1] = Player.getMANA();
	valueOfStatistic[3] = Player.getEXP();
}


HUD::~HUD()
{
}


void HUD::update()
{
	Vector2f posP = Player.getConstSprite().getPosition();

	int gold = Player.getGold();
	std::string gold_1 = "G: ";
	gold_1 += std::to_string(gold);
	Gold_1.setString(gold_1);
	//Gold_1.setPosition(screen.getCenter().x - screen.getSize().x / 2, screen.getCenter().y - screen.getSize().y / 2);
	
	Gold_1.setPosition(posP.x, posP.y - 60);
	
	int statisticMax[] = { Player.getMaxHp(), Player.getMaxMana(), 100 };
	float statisticCurrent[] = { Player.getHP(), Player.getMANA() - 20, 65 };

	for (int i = 0; i < 3; i++)
	{
		if (valueOfStatistic[i] > statisticCurrent[i])
		{
			valueOfStatistic[i]--;
		}
		else if (valueOfStatistic[i] < statisticCurrent[i])
		{
			valueOfStatistic[i]++;
		}
		float statisticProc = valueOfStatistic[i] / statisticMax[i];
		Vector2f size(Player.getConstSprite().getGlobalBounds().width * statisticProc, 6);
		statistic[i].setPosition(posP.x, posP.y - 40 + (i*(size.y + 2)));
		statistic[i].setSize(size);
	}


}